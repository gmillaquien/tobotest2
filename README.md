# Consultas hacia poder Judicial Laboral

Requerimientos Mínimos

-------------------------

Este script  realiza  las consultas hacia la pagina https://laboral.pjud.cl/ consultando cada juzgado por cada  personaa ingresada en el archivo excel.

- Instalar Python 2.7   [Descargar ](https://www.python.org/downloads/)

- Verificar que las variables de entorno se encuentre "C:\Python27;C:\Python27\Scripts;", las  cuales se pueden  agregar cuando se instala Python.
- Instalar Robot Framework :

    ``` pip install robotframework ```
- Revisar si quedo todo bien instalado:

     ``` robot --version ```

     ``` pip install -U wxPython ```

- Instalar RIDE:

    ``` pip install -U wxPython ```

Librerias necesarias para que el script funcione correctamente

-------------------------

- SeleniumLibrary

    ``` pip install robotframework-seleniumlibrary ```

- ExcelLibrary

     ``` pip install robotframework-excellibrary ```

- clipboard

    ``` pip install clipboard ```

-------------------------------------

Para terminar la instalación, debemos descargar el driver correspondiente a nuestro navegador, por defecto este script utiliza el driver de google chrome.


![Version Chrome](/img/version.png)  

De esta página descargamos el driver correspondiente y validamos que  sea correspondiente a la versión instalada en nuestro equipo.

 [Descargar  Driver Chrome ](https://chromedriver.chromium.org/downloads)


![Descarga Driver ](/img/descargaDriver.png)  

-------------------------------------

Para  que el script pueda guardar sin problemas en el archivo excel, se debe reemplzar en la ruta  C:\Python27\Lib\site-packages\ExcelLibrary, el contenido del rar que se adjunta  en los archivos del proyecto.


![Version Chrome](/img/contenido.png)  



-------------------------------------

## Ejecución del Script

Se inicia Ride y se carga el proyecto.


Se verifica la información que se desea consultar, la cual debe estar en un excel con el nombre **Nombres.xls** dentro de la carpeta resultado.


 
![Version Chrome](/img/nombres.png)  


Cuando se ejecute el Script se  realiza un archivo con el resultado de la consulta.

![Version Chrome](/img/ride.png)  


El  resultado se generar en base a las coincidencias entre los rut del excel y los rut del  de la página https://laboral.pjud.cl/ .

El Script generara un excel por cada coincidencia.

![Version Chrome](/img/resultado.png)  